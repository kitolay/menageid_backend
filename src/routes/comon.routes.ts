import express, { response } from "express";
import { userAuthentification } from "../controllers/authent.controller";
import { getCards, getDetailData } from "../controllers/card.controller";
import { saveData, SyncSaveData } from "../controllers/formdata.controller";
import {
  getCommune,
  getDistrict,
  getFokontany,
  getProvince,
  getRegion,
} from "../controllers/place.controller";
import {
  sendResetMail,
  chekUserPass,
  resetMail,
} from "../controllers/reset.controller";
import { imageFilter, STORAGE } from "../controllers/services";
import { authenticateToken } from "../Middleware/tokken.middleware";
import multer from "multer";
import { getStatistics } from "../controllers/stats.controller";
import { getDataToSync, getTableDataByName, getTableDatataByNameById, imageToBase64 } from "../controllers/sync.controller";

const router = express.Router();
const upload = multer({ storage: STORAGE, fileFilter: imageFilter }).single(
  "profile_pic"
);

//menageid routes
router.post("/authent", userAuthentification);
router.post("/recover", sendResetMail);
router.post("/checkReqPass", chekUserPass);
router.post("/resetpass", resetMail);
router.post("/card-list", authenticateToken, getCards);
router.post("/saveData", authenticateToken, upload, saveData);
router.get("/getprovince", authenticateToken, getProvince);
router.get("/getDetails", authenticateToken, getDetailData);
router.post("/getregion", authenticateToken, getRegion);
router.post("/getdistrict", authenticateToken, getDistrict);
router.post("/getcommune", authenticateToken, getCommune);
router.post("/getfokontany", authenticateToken, getFokontany);
router.get("/statistics", authenticateToken, getStatistics);
router.get("/syncdata-mobile", getDataToSync);
router.post("/sync-table-data", getTableDataByName);
router.post("/sync-data-by-table-by-id", getTableDatataByNameById);
router.post("/sync-saveData", authenticateToken, upload, SyncSaveData);
router.get("/image-base64", imageToBase64);
router.get("/contest",(_req, response)=>{
  response.send({state: true, message: 'connected'});
});
export default router;
