import jwt from "jsonwebtoken";
import { Response, Request, NextFunction } from "express";

export function authenticateToken(req: Request, res: Response, next: NextFunction) {
  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]

  if (token == null) return res.sendStatus(401)

  jwt.verify(token, process.env.TOKEN_SECRET ?? "P1tTLh6MjEkWthA6ouK199gVAVQkIqqW" as string, (err: any) => {
    console.log(err)
    if (err) return res.sendStatus(403)
    //req.user = user
    next()
  })
}