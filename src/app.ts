import express from 'express';
import cors from 'cors'
import bodyParser from 'body-parser'
import helmet from 'helmet'
import router from './routes/comon.routes'
import dotenv from 'dotenv'

const app = express();
dotenv.config();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(helmet())
app.use('/datacollect-server-test-api',router)
app.use(express.static(__dirname + '../public'));

const port = process.env.PORT || 3022;

app.listen(port, () => {
  console.log(`Collect Data backend app listening at port ${port}.`);
});