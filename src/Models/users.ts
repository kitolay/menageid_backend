import { Database } from "../Database/config";
import { CommonModel, ErrorMessage } from "./common";
const tabble_name = 'user_data'

export interface UserData extends CommonModel {
  name?: number;
  prename?: number;
  user_name?: string;
  email?: string;
  phone?: string;
  superviseur?: number;
}

export class UserDataModel{
    data_base: Database
    constructor(){
        this.data_base = new Database()
    }

    getByAny(ref:any):Promise<UserData[]> | ErrorMessage | null {
        if(this.data_base.connect()) {
            return this.data_base.getByAny(tabble_name,ref)
        }else return ({
            message: "Db connection error"
        })
    }
    updateByAny(ref:any, newData: any): Promise<UserData> | ErrorMessage | null {
        if(this.data_base.connect()) {
            return this.data_base.updateByAny(tabble_name,ref,newData)
        }else return ({
            message: "Db connection error"
        })
    }
    close(){
        this.data_base.disconnect()
    }
}
