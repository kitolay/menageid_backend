export interface AuthentBody {
  uname: string;
  upass: string;
}

export interface MailResetBody {
  email: string;
}

export interface PassResetBody {
  user: string;
  ref: string;
  newpass?: string;
  code?: string;
}
