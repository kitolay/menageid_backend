import { Database } from "../Database/config";
import { CommonModel, ErrorMessage } from "./common";
const tabble_name = "card_item";

export interface CardItem extends CommonModel {
  nom: string;
  prenom: string;
  nis: string;
  cdf: any;
  adress: string;
  pict: string;
  coderef: string;
  barref: string;
}

export class CardItemModel {
  data_base: Database;
  detail_list: string[] = [
    "enqueteur_data",
    "ig_data",
    "id_data",
    "ch_data",
    "ap_data",
    "srpt_data",
    "da_data",
    "acaa_data",
    "educres_data",
    "ep_data",
    "hmc_data",
    "validatiaon_data"
  ];
  id1_data:string = "id1_data";

  constructor() {
    this.data_base = new Database();
  }
  add(newCard: CardItem): Promise<CardItem>  | ErrorMessage | null {
    if (this.data_base.connect()) {
      return this.data_base.insertItem(tabble_name, newCard);
    } else
      return {
        message: "Db connection error",
      };
  }
  all(): Promise<CardItem[]> | ErrorMessage | null {
    if (this.data_base.connect()) {
      return this.data_base.getAll(tabble_name);
    } else
      return {
        message: "Db connection error",
      };
  }
  filtered(page: number, lim: number, ref: any) {
    if (this.data_base.connect()) {
      return this.data_base.getAllByFilter(tabble_name, page, lim, ref);
    } else
      return {
        message: "Db connection error",
      };
  }
  getById(id: number): Promise<CardItem[]> | ErrorMessage | null {
    if (this.data_base.connect()) {
      return this.data_base.getByAny(tabble_name,{id:id})
    } else
      return {
        message: "Db connection error",
      };
  }

  getDetailList(id: number): Promise<any> {
    let result_data:any = {};
    return new Promise((res, rej)=>{
      this.getByTableName(0,this.detail_list.length,id,this.detail_list,result_data,res)
    })
  }

  private getByTableName(i:number, lim: number,item_id:number, list: string[], container: any, res:(value:void)=>void){
    if(i<lim){
      this.data_base.getByAny(list[i],{refId: item_id}).then(resp=>{
        if(resp && resp.length > 0){
          container[list[i]] = resp[0]
          this.getByTableName(i+1,lim,item_id,list,container,res)
        }else this.getID1data(item_id, container, res)
      })
    }else this.getID1data(item_id, container, res)
  }

  private getID1data(itemId: number, container: any, res:(value:void)=>void){
    this.data_base.getByAny(this.id1_data,{refId: itemId}).then(resp=>{
      if(resp && resp.length > 0){
        container[this.id1_data] = resp
        res(container)
      }else res(container)
    })
  }

  close() {
    this.data_base.disconnect();
  }
}
