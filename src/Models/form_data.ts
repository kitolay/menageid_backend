import { Database } from "../Database/config";
import { CardItem, CardItemModel } from "./card";

export interface Id1Data {
  nom?: any;
  prenom?: any;
  surnom?: any;
  resstat?: any;
  sex?: any;
  age?: any;
  cpan?: any;
  cin?: { statue?: any; value?: any };
  enfu?: any;
  idenfu?: any;
  lienCDM?: any;
  statueM?: any;
  menageMere?: any;
  menageMereId?: any;
  orphan?: any;
  childMenage?: any;
  pregnante?: any;
}
export interface FormDataData {
  enqueteur_data: {
    name?: any;
    prename?: any;
    cin?: any;
    id?: any;
    sup_name?: any;
    sup_prename?: any;
    start_date?: Date;
  };
  ig_data: {
    lotis?: any;
    province?: any;
    region?: any;
    district?: any;
    commune?: any;
    fokontany?: any;
    milieu?: any;
    numero?: any;
  };
  id_data: {
    cdfm: any;
    pict?: any;
    nom?: any;
    prenom?: any;
    surnom?: any;
    cin?: any;
    datedelcin?: any;
    cincon?: any;
    datedelcincon?: any;
  };
  ch_data: {
    housetype?: any;
    extwall?: { au?: boolean; lib?: any };
    plaf?: { au?: boolean; lib?: any };
    npiech?: any;
    npiecedor?: any;
    piecsurf?: any;
    p1?: any;
    p2?: any;
    p3?: any;
    p4?: any;
    plancer?: { au?: boolean; lib?: any };
    elect?: { au?: boolean; lib?: any };
    eau?: { au?: boolean; lib?: any };
    toilet?: { au?: boolean; lib?: any };
  };
  ap_data: {
    "Chaise (avec pied et dossier)": { state: boolean; qte?: any };
    "Table (avec pied, bien droit)": { state: boolean; qte?: any };
    Natte: { state: boolean; qte?: any };
    "Lampe pétrole": { state: boolean; qte?: any };
    "Poste radio": { state: boolean; qte?: any };
    "Radio avec cassette, cd, mp3..": { state: boolean; qte?: any };
    Téléviseur: { state: boolean; qte?: any };
    "Lecteur CD DVD": { state: boolean; qte?: any };
    Bicyclette: { state: boolean; qte?: any };
    Puits: { state: boolean; qte?: any };
    Irrigation: { state: boolean; qte?: any };
    "Téléphone portable": { state: boolean; qte?: any };
    "Charrue (traction animale)": { state: boolean; qte?: any };
    "Charrette (traction animale)": { state: boolean; qte?: any };
    "Herse (traction animale)": { state: boolean; qte?: any };
    Bèche: { state: boolean; qte?: any };
    Stockage: { state: boolean; qte?: any };
    "Champ ou rizière (non métayage)": { state: boolean; qte?: any };
    Zébu: { state: boolean; qte?: any };
    Porc: { state: boolean; qte?: any };
    "Chèvre/mouton": { state: boolean; qte?: any };
    "Poulet gasy": { state: boolean; qte?: any };
    Rûche: { state: boolean; qte?: any };
    "Matériel de pêche: pirogue, filet": { state: boolean; qte?: any };
  };
  srpt_data: {
    sr: {
      "Salaire de la fonction publique"?: any;
      "Salaire du secteur privé"?: any;
      "Autre emploi"?: any;
      "Revenus agricoles"?: any;
      "Revenus d'artisanat"?: any;
      "Revenus du petit commerce"?: any;
      "Transfert social"?: any;
      "Envoi de fonds"?: any;
      Don?: any;
    };
    qcmrar?: any;
    pt: {
      "Superficie possédée": any;
      "Superficie louée ou empruntée"?: any;
      "Superficie cultivée"?: any;
    };
    tc1: { type?: any; qte?: any };
    tc2: { type?: any; qte?: any };
    tc3: { type?: any; qte?: any };
  };
  da_data: {
    main: {
      Semences: { mantant?: any; fourniseur?: any };
      "Main d’œuvre (0 si main d'œuvre gratuit)": {
        mantant?: any;
        fourniseur?: any;
      };
      Pesticides: { mantant?: any; fourniseur?: any };
      Fertilisants: { mantant?: any; fourniseur?: any };
      "Achat de matériel et équipements": { mantant?: any; fourniseur?: any };
      "Location de matériel et équipements": {
        mantant?: any;
        fourniseur?: any;
      };
    };
    autre: [{ name?: any; mantant?: any; fourniseur?: any }];
  };
  acaa_data: {
    aac: boolean;
    aacname: { AA02: any; AA03: any; AA04: any };
    afil: boolean;
    imf: boolean;
    imfname?: any;
    imfmontant?: any;
    imfprop?: any;
    notimf?: any;
    authercred?: any;
    commercant: boolean;
    autre?: any;
  };
  id1_data: Id1Data[];
  educres_data: {
    primary?: any;
    nombredanner?: any;
    goscool?: any;
    malagasy?: any;
    malagasyecrit?: any;
    sixmouth?: any;
    annee?: any;
  };
  ep_data: {
    principa_empl?: any;
  };
  hmc_data: {
    lunette?: any;
    audition?: any;
    walking?: any;
    brain?: any;
    selfcare?: any;
    maternallng?: any;
    caring?: any;
    chronicdeaseas?: any;
    chronicdeaseasauth?: any;
    diffic?: any;
  };
  validatiaon_data: {
    chef?: any;
    persone?: any;
    quality?: any;
  };
}

export class FormDataModel {
  data_base: Database;
  constructor() {
    this.data_base = new Database();
  }

  insertAllDetails(allData: any, refId: number, sync?: boolean): Promise<any> {
    if (this.data_base.connect()) {
      const all_data_keys = Object.keys(allData);
      const all_data_value = Object.values(allData);

      return new Promise((res, rej) => {
        this.insertTableItem(
          0,
          all_data_keys.length,
          all_data_keys,
          all_data_value,
          refId,
          res,
          sync
        );
      });
    } else
      return new Promise((res, rej) => {
        res({
          error: true,
          message: "Db connection error",
        });
      });
  }

  insertTableItem(
    i: number,
    lim: number,
    keys: any,
    values: any,
    refid: number,
    res: (value: any) => void,
    sync?: boolean
  ) {
    if (i < lim) {
      if (keys[i] === "id1_data") {
        this.inserAutherItem(
          0,
          values[i].length,
          values[i],
          keys[i],
          refid,
          res,
          i,
          lim,
          keys,
          values,
          sync
        );
      } else {
        this.data_base
          .insertItem(keys[i], { ...values[i], refId: refid })
          .then((insrep) => {
            if (insrep.message) {
              res({
                error: true,
                message: insrep,
              });
            } else {
              this.insertTableItem(i + 1, lim, keys, values, refid, res, sync);
            }
          });
      }
    } else {
      if (sync) {
        this.getDetailData(refid, res);
      } else
        res({
          error: false,
          message: "Element addede.",
        });
    }
  }

  inserAutherItem(
    ai: number,
    alim: number,
    avalues: any,
    atable: string,
    aref: number,
    ares: (value: any) => void,
    nexti: number,
    nextlim: number,
    nextkey: any,
    nexvalue: any,
    sync?: boolean
  ) {
    if (ai < alim) {
      this.data_base
        .insertItem(atable, { ...avalues[ai], refId: aref })
        .then((arep) => {
          if (arep.message) {
            ares({
              error: true,
              message: arep,
            });
          } else {
            this.inserAutherItem(
              ai + 1,
              alim,
              avalues,
              atable,
              aref,
              ares,
              nexti,
              nextlim,
              nextkey,
              nexvalue,
              sync
            );
          }
        });
    } else
      this.insertTableItem(
        nexti + 1,
        nextlim,
        nextkey,
        nexvalue,
        aref,
        ares,
        sync
      );
  }
  
  getDetailData(cardId: number, res: (value: any) => void) {
    const cards = new CardItemModel();
    const card_item = cards.getById(cardId) as any;
    if (typeof card_item.then === "function") {
      card_item.then((card_res: CardItem[]) => {
        if (card_res[0] && card_res[0].id) {
          const card_details = cards.getDetailList(cardId) as any;
          if (typeof card_details.then === "function") {
            card_details.then((cdetail: any) => {
              if (cdetail && cdetail !== {}) {
                res({ ...card_res[0], details: cdetail });
              } else {
                cards.close();
                res(card_res);
              }
            });
          } else {
            cards.close();
            res(card_res);
          }
        } else {
          cards.close();
          res(card_res);
        }
      });
    } else {
      cards.close();
      res(card_item);
    }
  }

  close() {
    this.data_base.disconnect();
  }
}
