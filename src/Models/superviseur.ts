import { Database } from "../Database/config";
import { CommonModel, ErrorMessage } from "./common";
const tabble_name = "supervisor_data";
export interface SuperviseurData extends CommonModel {
  name?: string;
  prename?: string;
  email?: string;
  phone?: string;
}

export class SuperviseurModel {
  data_base: Database;
  constructor() {
    this.data_base = new Database();
  }
  getByAny(ref: any): Promise<SuperviseurData[]> | ErrorMessage | null {
    if (this.data_base.connect()) {
      return this.data_base.getByAny(tabble_name, ref);
    } else
      return {
        message: "Db connection error",
      };
  }
  updateByAny(ref: any, newData: any): Promise<SuperviseurData> | ErrorMessage | null {
    if (this.data_base.connect()) {
      return this.data_base.updateByAny(tabble_name, ref, newData);
    } else
      return {
        message: "Db connection error",
      };
  }
  close() {
    this.data_base.disconnect();
  }
}
