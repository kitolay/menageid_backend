import { Database } from "../Database/config"
import { ErrorMessage } from "./common";

const provincedb = 'liste_province'
const regiondb = 'liste_region'
const districtdb = 'list_district'
const commundb = 'list_commune'
const fokontanydb = 'list_fokontany'

export interface ProvinceData{
    id: number;
    refId: number;
    lib: string;
}

export interface RegionData extends ProvinceData{
    prov_id: number;
}

export interface DistrictData extends RegionData{
    reg_id: number;
}

export interface CommuneData extends DistrictData{
    dis_id: number;
}

export interface FokontanyData extends CommuneData{
    com_id: number;
}

export class PlaceModel{
    data_base: Database
    constructor(){
        this.data_base = new Database()
    }

    getProvince():Promise< ProvinceData[]>  | ErrorMessage | null{
        if(this.data_base.connect()) {
            return this.data_base.getAll(provincedb)
        }else return ({
            message: "Db connection error"
        })
    }
    getRegion(regionfilter: any):Promise< RegionData[]>  | ErrorMessage | null{
        if(this.data_base.connect()) {
            return this.data_base.getByAny(regiondb,regionfilter)
        }else return ({
            message: "Db connection error"
        })
    }
    getDistrict(districtfilter: any):Promise< DistrictData[]>  | ErrorMessage | null{
        if(this.data_base.connect()) {
            return this.data_base.getByAny(districtdb,districtfilter)
        }else return ({
            message: "Db connection error"
        })
    }
    getComunne(comunfilter: any):Promise< CommuneData[]>  | ErrorMessage | null{
        if(this.data_base.connect()) {
            return this.data_base.getByAny(commundb,comunfilter)
        }else return ({
            message: "Db connection error"
        })
    }
    getFokontany(fokotanyfilter: any):Promise< FokontanyData[]>  | ErrorMessage | null{
        if(this.data_base.connect()) {
            return this.data_base.getByAny(fokontanydb,fokotanyfilter)
        }else return ({
            message: "Db connection error"
        })
    }

    close(){
        this.data_base.disconnect()
    }
}