export interface CommonModel {
  id?: number;
  createdAt?: Date;
  updatedAt?: Date;
}
export interface ErrorMessage {
  message?: string;
  query?: string;
}
