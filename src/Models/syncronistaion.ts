import { Database } from "../Database/config";
import { ErrorMessage } from "./common";

export interface SyndTableNameData {
  table_name: string;
}

export class SyncroData {
  data_base: Database;
  constructor() {
    this.data_base = new Database();
  }
  getTableList(): Promise<SyndTableNameData[]> | ErrorMessage | null {
    if (this.data_base.connect()) {
      return this.data_base.getTables();
    } else
      return {
        message: "Db connection error",
      };
  }
  getAllIds(tableListes: string[]): Promise<any> | ErrorMessage | null {
    if (this.data_base.connect()) {
      return this.data_base.getSyncIds(tableListes);
    } else
      return {
        message: "Db connection error",
      };
  }

  getDatataByTable(tableListes: string[]): Promise<any> | ErrorMessage | null {
    if (this.data_base.connect()) {
      return this.data_base.getSyncIds(tableListes, true);
    } else
      return {
        message: "Db connection error",
      };
  }

  getDatataByTableById(tableDataListes: {table:string, ids: number[]}[]): Promise<any> | ErrorMessage | null {
    if (this.data_base.connect()) {
      const tableListes = tableDataListes.map((tdata)=> tdata.table);
      const tableDatas = tableDataListes.map((tdata)=> tdata.ids);
      return this.data_base.getSyncIds(tableListes, true, tableDatas);
    } else
      return {
        message: "Db connection error",
      };
  }

  close() {
    this.data_base.disconnect();
  }
}
