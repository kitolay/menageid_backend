import { Database } from "../Database/config";
import { CommonModel, ErrorMessage } from "./common";
const tabble_name = 'reset_code_temp'

export interface CodeRecover extends CommonModel {
  user: string;
  mail: string;
  code: string;
  expire: Date;
  ref: string;
}

export class CodeRecoverModel {
    data_base: Database
    constructor(){
        this.data_base = new Database()
    }
    getByAny(ref:any):Promise<CodeRecover[]> | ErrorMessage | null {
        if(this.data_base.connect()) {
            return this.data_base.getByAny(tabble_name,ref)
        }else return ({
            message: "Db connection error"
        })
    }
    insertItem(data: any):Promise<any> | ErrorMessage | null{
        if(this.data_base.connect()) {
            return this.data_base.insertItem(tabble_name,data)
        }else return ({
            message: "Db connection error"
        })
    }
    close(){
        this.data_base.disconnect()
    }
}
