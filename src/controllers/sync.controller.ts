import { Response, Request } from "express";
import { body_error, no_data_error } from "../common/Response";
import { SyncroData, SyndTableNameData } from "../Models/syncronistaion";
import { generateAccessToken, imageToBase64Func } from "./services";

export function getDataToSync(req: Request, response: Response) {
  const syn_data = new SyncroData();
  const res = syn_data.getTableList() as any;
  if (typeof res.then === "function") {
    res.then((resp: SyndTableNameData[]) => {
      if (resp && resp.length > 0) {
        const all_ids = syn_data.getAllIds(
          resp.map((dres) => dres.table_name)
        ) as any;
        if (typeof all_ids.then === "function") {
          all_ids.then((result: any) => {
            syn_data.close();
            response.send({
              allIds: result,
              tokken: generateAccessToken({uname: 'syncTokken'})
            });
          });
        } else {
          syn_data.close();
          response.status(400).send(no_data_error(all_ids));
        }
      } else {
        syn_data.close();
        response.status(400).send(no_data_error(resp));
      }
    });
  } else {
    syn_data.close();
    response.status(400).send(res);
  }
}

export function getTableDataByName(req: Request, response: Response) {
  if (req.body) {
    const syn_data = new SyncroData();
    const res = syn_data.getDatataByTable(req.body) as any;
    if (typeof res.then === "function") {
      res.then((result: any[]) => {
        if (result && result.length > 0) {
          syn_data.close();
          response.send(result);
        } else {
          syn_data.close();
          response.status(400).send(no_data_error(result));
        }
      });
    } else {
      syn_data.close();
      response.status(400).send(res);
    }
  } else {
    response.status(400).send(body_error);
  }
}

export function getTableDatataByNameById(req: Request, response: Response) {
  if (req.body) {
    const syn_data = new SyncroData();
    const res = syn_data.getDatataByTableById(req.body) as any;
    if (typeof res.then === "function") {
      res.then((result: any[]) => {
        if (result && result.length > 0) {
          syn_data.close();
          response.send(result);
        } else {
          syn_data.close();
          response.status(400).send(no_data_error(result));
        }
      });
    } else {
      syn_data.close();
      response.status(400).send(res);
    }
  } else {
    response.status(400).send(body_error);
  }
}

export const imageToBase64 = (req: Request, response: Response) =>{
  if(req.query.name){
    response.send(imageToBase64Func(req.query.name as string));
  }else {
    response.status(400).send(body_error);
  }
}
