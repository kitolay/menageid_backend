import { no_data_error, expired_data, body_error } from "./../common/Response";
import { Response, Request } from "express";
import { MailResetBody, PassResetBody } from "../Models/Request";
import { UserData, UserDataModel } from "../Models/users";
import { CodeRecover, CodeRecoverModel } from "../Models/recover_code";
import { makeid } from "./services";
import { sendMail } from "./services";

export function sendResetMail(req: Request, response: Response) {
  var date = new Date();
  if (req.body) {
    const req_data = req.body as MailResetBody;
    const user = new UserDataModel();
    const code_data = new CodeRecoverModel();
    const res = user.getByAny(req_data) as any;
    if (typeof res.then === "function") {
      res.then((result: UserData[]) => {
        if (result && result.length > 0) {
          const c_user = result[0];
          const code_dt = makeid(6);
          const ref_cod = makeid(6);

          const cod_qres = code_data.insertItem({
            user: c_user.user_name,
            mail: c_user.email,
            code: code_dt,
            expire: date.setDate(date.getDate() + 1),
            ref: ref_cod,
            createdAt: date,
            updatedAt: date,
          }) as any;
          if (typeof cod_qres.then === "function") {
            cod_qres.then((cres: any) => {
              sendMail(
                process.env.EMAIL_SENDER ?? "recover@datacollect.mg",
                [result[0].email ?? ""],
                "Le code de reinisialisation de votre mot de pass est: " +
                  code_dt,
                "CODE_DE_RECUPERATION"
              ).then((mailres) => {
                response.send({
                  user: c_user.user_name,
                  mail: c_user.email,
                  expire: date.setDate(date.getDate() + 1),
                  ref: ref_cod,
                });
                code_data.close();
              });
            });
          } else {
            code_data.close();
            response.send(cod_qres);
          }
        } else response.send(no_data_error(res));
        user.close();
      });
    } else {
      user.close();
      response.send(res);
    }
  } else {
    response.send(body_error);
  }
}
export function chekUserPass(req: Request, response: Response) {
  if (req.body) {
    const req_data = req.body as PassResetBody;
    const code_data = new CodeRecoverModel();
    const res = code_data.getByAny({
        user: req_data.user,
        ref: req_data.ref,
        code: req_data.code
    }) as any;
    if (typeof res.then === "function") {
      res.then((result: CodeRecover[]) => {
        if (result && result.length > 0) {
          const res_cod = result[0];
          const expirdate = new Date(res_cod.expire);
          if (expirdate < new Date()) {
            response.send(req_data);
          } else response.send(expired_data);
          code_data.close();
        } else response.send(no_data_error(res));
        code_data.close();
      });
    } else {
      code_data.close();
      response.send(res);
    }
  } else {
    response.send(body_error);
  }
}

export function resetMail(req: Request, response: Response) {
  if (req.body) {
    const req_data = req.body as PassResetBody;
    const code_data = new CodeRecoverModel();
    const res = code_data.getByAny({
        user: req_data.user,
        ref: req_data.ref,
        code: req_data.code
    }) as any;
    if (typeof res.then === "function") {
      res.then((result: CodeRecover[]) => {
        if (result && result.length > 0) {
          const res_cod = result[0];
          const expirdate = new Date(res_cod.expire);
          if (expirdate < new Date()) {
            const user = new UserDataModel();
            const updated = user.updateByAny(
              { user_name: req_data.user },
              { pass: req_data.newpass }
            ) as any;
            if (typeof updated.then === "function") {
              updated.then((ures: any) => {
                response.send(ures);
                user.close();
              });
            } else {
              user.close();
              response.send(updated);
            }
          } else response.send(expired_data);
          code_data.close();
        } else response.send(no_data_error(res));
        code_data.close();
      });
    } else {
      code_data.close();
      response.send(res);
    }
  } else {
    response.send(body_error);
  }
}
