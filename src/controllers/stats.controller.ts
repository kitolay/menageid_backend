import { no_data_error } from "./../common/Response";
import { Response, Request } from "express";
import { body_error } from "../common/Response";

export function getStatistics(req: Request, response: Response) {
  if (req.query.id) {
    const state_id = parseInt(req.query.id as string);
    switch (state_id) {
      case 0:
        response.send(options);
        break;
      case 1:
        response.send(optiondata1);
        break;
      case 2:
        response.send(option2);
        break;
      case 3:
        response.send(option4);
        break;
      case 4:
        response.send(option1);
        break;
      case 5:
        response.send(option1a);
        break;
      case 6:
        response.send(option1b);
        break;
      case 7:
        response.send(option1c);
        break;
      case 8:
        response.send(option1d);
        break;
      case 9:
        response.send(option2);
        break;
      case 10:
        response.send(option3);
        break;
      case 11:
        response.send(option5);
        break;
      default:
        response.status(400).send(no_data_error("no stateistics found"));
        break;
    }
  } else {
    response.status(400).send(body_error);
  }
}

const options = {
  //legend: {
  //data: ["Taux de pauvreté par région"],
  //align: "left",
  //},
  tooltip: {
    trigger: "axis",
    axisPointer: {
      type: "shadow",
    },
  },
  grid: {
    left: "3%",
    right: "4%",
    bottom: "3%",
    containLabel: true,
  },
  xAxis: [
    {
      type: "category",
      axisLabel: { interval: 0 },
      axisTick: {
        alignWithLabel: true,
      },
      data: [
        "Itasy",
        "\nAmoron'i\nMania",
        "Vakinankaratra",
        "\nBongolava",
        "Analamanga",
        "\nAtsimo-\nAtsinanana",
        "Diana",
        "\nAndroy",
        "Vatovavy\nFitovinany",
        "\nHaute\nMatsiatra",
        "Analanjirofo",
        "\nAtsinanana",
        "Sava",
        "\nAnôsy",
        "Ihorombe",
        "\nBetsiboka",
        "Boeny",
        "\nAlaotra\nMangoro",
        "Melaky",
        "\nMenabe",
        "Sofia",
        "\nAtsimo-\nAndrefana",
      ],
    },
  ],
  yAxis: [
    {
      type: "value",
    },
  ],
  series: [
    {
      name: "Taux de pauvreté",
      type: "bar",
      data: [
        75.27, 73.44, 18.52, 46.97, 34.39, 92.30, 71.90, 75.38,
        14.54, 12.31, 10.63, 13.05, 10.07, 69.00, 32.07,
        30.14, 82.13, 10.54, 29.74, 60.81, 12.80, 13.52,
      ],
      //animationDelay: function (idx) { return idx * 10; },
    },
    // {
    //   name: "superficie (km²)",
    //   type: "bar",
    //   data: [
    //     6993, 16141, 16599, 16688, 16911, 18863, 19266, 19317, 19605, 21080,
    //     21930, 21934, 25518, 25731, 26391, 30025, 31046, 31948, 38852, 46121,
    //     50100, 66236,
    //   ],
    //   animationDelay: (idx: any) => idx * 10 + 100,
    // },
  ],
  //animationEasing: "elasticOut",
  //animationDelayUpdate: function (idx) { return idx * 5; },
};

const optiondata1 = {
  title: {
    text: "Répartition sphérique",
    left: "center",
  },
  tooltip: {
    trigger: "item",
    formatter: "{a} <br/>{b} : {c} ({d}%)",
  },
  legend: {
    bottom: 5,
    left: "center",
    data: [
      "Mahajanga",
      "Antsiranana",
      "Toamasina",
      "Toliara",
      "Antananarivo",
      "Antsirabe",
    ],
  },
  series: [
    {
      name: "Effectifs",
      type: "pie",
      radius: "85%",
      label: {
        position: "inner",
        formatter: "{d}%",
        color: "#fff",
        textStyle: {
          fontSize: 10,
          fontWeight: 500,
        },
      },
      data: [
        { value: 335, name: "Mahajanga" },
        { value: 310, name: "Antsiranana" },
        { value: 234, name: "Toamasina" },
        { value: 135, name: "Toliara" },
        { value: 1248, name: "Antananarivo" },
        { value: 300, name: "Antsirabe" },
      ],
      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: "rgba(0, 0, 0, 0.5)",
        },
      },
    },
  ],
};

const option2 = {
  tooltip: {
    trigger: "axis",
    axisPointer: {
      // 坐标轴指示器，坐标轴触发有效
      type: "shadow", // 默认为直线，可选为：'line' | 'shadow'
    },
  },
  legend: {
    data: [
      "Mahajanga",
      "Antsiranana",
      "Toamasina",
      "Toliara",
      "Antananarivo",
      "Antsirabe",
    ],
  },
  grid: {
    left: "3%",
    right: "4%",
    bottom: "3%",
    containLabel: true,
  },
  xAxis: [
    {
      type: "category",
      data: ["2015", "2016", "2017", "2018", "2019", "2020", "2021"],
    },
  ],
  yAxis: [
    {
      type: "value",
    },
  ],
  series: [
    {
      name: "Mahajanga",
      type: "bar",
      data: [320, 332, 301, 334, 390, 330, 320],
    },
    {
      name: "Antsiranana",
      type: "bar",
      stack: "广告",
      data: [120, 132, 101, 134, 90, 230, 210],
    },
    {
      name: "Toamasina",
      type: "bar",
      stack: "广告",
      data: [220, 182, 191, 234, 290, 330, 310],
    },
    {
      name: "Toliara",
      type: "bar",
      stack: "广告",
      data: [150, 232, 201, 154, 190, 330, 410],
    },
    {
      name: "Antananarivo",
      type: "bar",
      data: [862, 1018, 964, 1026, 1679, 1600, 1570],
      markLine: {
        lineStyle: {
          type: "dashed",
        },
        data: [[{ type: "min" }, { type: "max" }]],
      },
    },
    {
      name: "Antsirabe",
      type: "bar",
      barWidth: 5,
      stack: "搜索引擎",
      data: [620, 732, 701, 734, 1090, 1130, 1120],
    },
  ],
};

const option4 = {
  title: {
    text: "Ménages",
  },
  tooltip: {
    trigger: "axis",
  },
  legend: {
    data: [
      "Mahajanga",
      "Antsiranana",
      "Toamasina",
      "Toliara",
      "Antananarivo",
      "Antsirabe",
    ],
    top: "35px",
  },
  grid: {
    left: "3%",
    right: "4%",
    bottom: "3%",
    containLabel: true,
    top: "100px",
  },
  toolbox: {
    feature: {
      saveAsImage: {},
    },
  },
  xAxis: {
    type: "category",
    boundaryGap: false,
    data: ["2015", "2016", "2017", "2018", "2019", "2020", "2021"],
  },
  yAxis: {
    type: "value",
  },
  series: [
    {
      name: "Mahajanga",
      type: "line",
      stack: "总量",
      data: [120, 132, 101, 134, 90, 230, 210],
    },
    {
      name: "Antsiranana",
      type: "line",
      stack: "总量",
      data: [220, 182, 191, 234, 290, 330, 310],
    },
    {
      name: "Toamasina",
      type: "line",
      stack: "总量",
      data: [150, 232, 201, 154, 190, 330, 410],
    },
    {
      name: "Toliara",
      type: "line",
      stack: "总量",
      data: [320, 332, 301, 334, 390, 330, 320],
    },
    {
      name: "Antananarivo",
      type: "line",
      stack: "总量",
      data: [820, 932, 901, 934, 1290, 1330, 1320],
    },
    {
      name: "Antsirabe",
      type: "line",
      stack: "总量",
      data: [820, 932, 901, 934, 1290, 1330, 1320],
    },
  ],
};
const option1 = {
  tooltip: {
    trigger: "item",
    formatter: "{a} <br/>{b}: {c} ({d}%)",
  },
  legend: {
    orient: "vertical",
    left: 10,
    data: [" ", ""],
  },
  series: [
    {
      name: "Ménage",
      type: "pie",
      radius: ["50%", "70%"],
      avoidLabelOverlap: false,
      label: {
        show: false,
        position: "center",
      },
      emphasis: {
        label: {
          show: true,
          fontSize: "30",
          fontWeight: "bold",
        },
      },
      labelLine: {
        show: false,
      },
      data: [
        { value: 17870, name: "Enquêtés" },
        { value: 2247, name: "Reste" },
      ],
    },
  ],
};

const option1a = {
  tooltip: {
    trigger: "item",
    formatter: "{a} <br/>{b}: {c} ({d}%)",
  },
  legend: {
    orient: "vertical",
    left: 10,
    data: [" ", ""],
  },
  series: [
    {
      name: "Ménage",
      type: "pie",
      radius: ["50%", "70%"],
      avoidLabelOverlap: false,
      label: {
        show: false,
        position: "center",
      },
      emphasis: {
        label: {
          show: true,
          fontSize: "30",
          fontWeight: "bold",
        },
      },
      labelLine: {
        show: false,
      },
      data: [
        { value: 17161, name: "Enquêtées" },
        { value: 1651, name: "Reste" },
      ],
    },
  ],
};

const option1b = {
  tooltip: {
    trigger: "item",
    formatter: "{a} <br/>{b}: {c} ({d}%)",
  },
  legend: {
    orient: "vertical",
    left: 10,
    data: [" ", ""],
  },
  series: [
    {
      name: "Ménage",
      type: "pie",
      radius: ["50%", "70%"],
      avoidLabelOverlap: false,
      label: {
        show: false,
        position: "center",
      },
      emphasis: {
        label: {
          show: true,
          fontSize: "30",
          fontWeight: "bold",
        },
      },
      labelLine: {
        show: false,
      },
      data: [
        { value: 7623, name: "Enquêtés" },
        { value: 1357, name: "Reste" },
      ],
    },
  ],
};

const option1c = {
  tooltip: {
    trigger: "item",
    formatter: "{a} <br/>{b}: {c} ({d}%)",
  },
  legend: {
    orient: "vertical",
    left: 10,
    data: [" ", ""],
  },
  series: [
    {
      name: "Ménage",
      type: "pie",
      radius: ["50%", "70%"],
      avoidLabelOverlap: false,
      label: {
        show: false,
        position: "center",
      },
      emphasis: {
        label: {
          show: true,
          fontSize: "30",
          fontWeight: "bold",
        },
      },
      labelLine: {
        show: false,
      },
      data: [
        { value: 12857, name: "Enquêtés" },
        { value: 498, name: "Reste" },
      ],
    },
  ],
};

const option1d = {
  tooltip: {
    trigger: "item",
    formatter: "{a} <br/>{b}: {c} ({d}%)",
  },
  legend: {
    orient: "vertical",
    left: 10,
    data: [" ", ""],
  },
  series: [
    {
      name: "Ménage",
      type: "pie",
      radius: ["50%", "70%"],
      avoidLabelOverlap: false,
      label: {
        show: false,
        position: "center",
      },
      emphasis: {
        label: {
          show: true,
          fontSize: "30",
          fontWeight: "bold",
        },
      },
      labelLine: {
        show: false,
      },
      data: [
        { value: 11977, name: "Enquêtés" },
        { value: 452, name: "Reste" },
      ],
    },
  ],
};

const option2a = {
  color: ["#3398DB"],
  tooltip: {
    trigger: "axis",
    axisPointer: {
      // 坐标轴指示器，坐标轴触发有效
      type: "shadow", // 默认为直线，可选为：'line' | 'shadow'
    },
  },
  grid: {
    left: "3%",
    right: "4%",
    bottom: "3%",
    containLabel: true,
  },
  xAxis: [
    {
      type: "category",
      data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
      axisTick: {
        alignWithLabel: true,
      },
    },
  ],
  yAxis: [
    {
      type: "value",
    },
  ],
  series: [
    {
      name: "直接访问",
      type: "bar",
      barWidth: "60%",
      data: [10, 52, 200, 334, 390, 330, 220],
    },
  ],
};

const option3 = {
  title: {
    text: "堆叠区域图",
  },
  tooltip: {
    trigger: "axis",
    axisPointer: {
      type: "cross",
      label: {
        backgroundColor: "#6a7985",
      },
    },
  },
  legend: {
    data: ["邮件营销", "联盟广告", "视频广告", "直接访问", "搜索引擎"],
  },
  toolbox: {
    feature: {
      saveAsImage: {},
    },
  },
  grid: {
    left: "3%",
    right: "4%",
    bottom: "3%",
    containLabel: true,
  },
  xAxis: [
    {
      type: "category",
      boundaryGap: false,
      data: ["周一", "周二", "周三", "周四", "周五", "周六", "周日"],
    },
  ],
  yAxis: [
    {
      type: "value",
    },
  ],
  series: [
    {
      name: "邮件营销",
      type: "line",
      stack: "总量",
      areaStyle: {},
      data: [120, 132, 101, 134, 90, 230, 210],
    },
    {
      name: "联盟广告",
      type: "line",
      stack: "总量",
      areaStyle: {},
      data: [220, 182, 191, 234, 290, 330, 310],
    },
    {
      name: "视频广告",
      type: "line",
      stack: "总量",
      areaStyle: {},
      data: [150, 232, 201, 154, 190, 330, 410],
    },
    {
      name: "直接访问",
      type: "line",
      stack: "总量",
      areaStyle: {},
      data: [320, 332, 301, 334, 390, 330, 320],
    },
    {
      name: "搜索引擎",
      type: "line",
      stack: "总量",
      label: {
        normal: {
          show: true,
          position: "top",
        },
      },
      areaStyle: {},
      data: [820, 932, 901, 934, 1290, 1330, 1320],
    },
  ],
};

const option5 = {
  tooltip: {
      trigger: 'axis',
      axisPointer: {
          type: 'shadow'
      }
  },
  grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
  },
  xAxis: {
      type: 'value',
      boundaryGap: [0, 0.01]
  },
  yAxis: {
      type: 'category',
      data: [
      "Itasy",
      "Amoron'iMania",
      "Vakinankaratra",
      "Bongolava",
      "Analamanga",
      "Atsimo-Atsinanana",
      "Diana",
      "Androy",
      "VatovavyFitovinany",
      "Haute Matsiatra",
      "Analanjirofo",
      "Atsinanana",
      "Sava",
      "Anôsy",
      "Ihorombe",
      "Betsiboka",
      "Boeny",
      "Alaotra Mangoro",
      "Melaky",
      "Menabe",
      "Sofia",
      "Atsimo-Andrefana",
    ]
  },
  series: [
      {
          name: 'Taux de pauvreté',
          type: 'bar',
          data: [
      75.2703, 73.4413, 18.52199, 46.9769, 34.39589, 92.3068, 71.9, 75.3832,
      14.54863, 12.31696, 10.63197, 13.05132, 10.07399, 69.0019, 32.0775,
      30.148, 82.1356, 10.54958, 29.7446, 60.8166, 12.80847, 13.52456,
    ]
      }
  ]
};
