import { Response, Request } from "express";
import { body_error, no_data_error } from "../common/Response";
import { AuthentBody } from "../Models/Request";
import { SuperviseurData, SuperviseurModel } from "../Models/superviseur";
import { UserData, UserDataModel } from "../Models/users";
import { generateAccessToken } from "./services";

export function userAuthentification(req: Request, response: Response) {
  if (req.body) {
    const req_data = req.body as AuthentBody;
    const user = new UserDataModel();
    const res = user.getByAny(req_data) as any;
    if (typeof res.then === "function") {
      res.then((result: UserData[]) => {
        if (result && result.length > 0) {
          const superviseur = new SuperviseurModel();
          const sup_res = superviseur.getByAny({ id: result[0].superviseur }) as any;
          if (typeof sup_res.then === "function") {
            sup_res.then((supres: SuperviseurData[]) => {
              if (supres && supres.length > 0) {
                superviseur.close()
                response.send({
                  user: result.map((itm) => {
                    let itm_sub = itm as any;
                    itm_sub.superviseur = supres[0];
                    delete itm_sub.pass;
                    return itm_sub;
                  })[0],
                  tokken: generateAccessToken({ uname: req_data.uname }),
                });
              } else {
                superviseur.close()
                response.send({
                  user: result.map((itm) => {
                    let itm_sub = itm as any;
                    itm_sub.superviseur = null;
                    delete itm_sub.pass;
                    return itm_sub;
                  })[0],
                  tokken: generateAccessToken({ uname: req_data.uname }),
                });
              }
            });
          } else {
            superviseur.close()
            response.send({
              user: result.map((itm) => {
                let itm_sub = itm as any;
                itm_sub.superviseur = null;
                delete itm_sub.pass;
                return itm_sub;
              })[0],
              tokken: generateAccessToken({ uname: req_data.uname }),
            });
          }
        } else response.status(400).send(no_data_error(res));
        user.close();
      });
    } else {
      user.close();
      response.send(res);
    }
  } else {
    response.status(400).send(body_error);
  }
}
