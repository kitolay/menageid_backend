import jwt from "jsonwebtoken";
import nodemailer from "nodemailer";
import multer from "multer";
import path from "path";
import fs from "fs";

const secret = process.env.TOKEN_SECRET ?? "P1tTLh6MjEkWthA6ouK199gVAVQkIqqW";
export const image_src =
  process.env.IMAGE_PLACE ?? "https://coursta.mg/menageid/images/";
export const public_image_src =
  process.env.PUBLIC_IMAGE_PLACE ?? "../../public_html/menageid/images";

export const STORAGE = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, public_image_src);
  },
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  },
});

export const imageToBase64Func = (imageName: string) => {
  var bitmap = fs.readFileSync(public_image_src + "/" + imageName);
  return new Buffer(bitmap).toString("base64");
};

export function imageFilter(req: any, file: any, cb: any) {
  // Accept images only
  if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
    req.fileValidationError = "Only image files are allowed!";
    return cb(new Error("Only image files are allowed!"), false);
  }
  cb(null, true);
}

export function generateAccessToken(user: { uname: string }) {
  return jwt.sign(user, secret, { expiresIn: 28800 });
}

export function makeid(length: number) {
  var result = [];
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result.push(
      characters.charAt(Math.floor(Math.random() * charactersLength))
    );
  }
  return result.join("");
}

export function sendMail(
  from: string,
  to: string[],
  message: string,
  subject?: string
): Promise<any> {
  //const testAccount = await nodemailer.createTestAccount();
  // return nodemailer.createTestAccount().then((testAccount):Promise<any> => {

  // });
  const transporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST ?? "mail.coursta.mg",
    port: parseInt(process.env.EMAIL_PORT ?? '587'),
    secure: true,
    auth: {
      user: process.env.EMAIL_USER ?? "mail.coursta.mg",
      pass: process.env.EMAIL_PASS ?? "hLu2s4nuLQ",
    },
  });

  return transporter.sendMail({
    from: from,
    to: to.join(", "),
    subject: subject ?? "no subject",
    text: message,
  });
}
