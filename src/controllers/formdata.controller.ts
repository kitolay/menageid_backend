import { Response, Request } from "express";
import { CardItemModel } from "../Models/card";
import { body_error, no_data_error } from "../common/Response";
import { FormDataData, FormDataModel } from "../Models/form_data";
import { image_src } from "./services";

export function saveData(req: Request, response: Response) {
  if (req.body && req.body.formData) {
    const form_data = JSON.parse(req.body.formData) as FormDataData;
    const cards = new CardItemModel();

    const PMT = Math.floor((Math.random() * 97) + 1)/100;
    const res = cards.add({
      nom: form_data.id_data.nom,
      prenom: form_data.id_data.prenom,
      nis: form_data.id_data.cin,
      cdf: form_data.id_data.cdfm ? 1 : 0,
      adress:
        (form_data.ig_data.lotis ?? "") +
        "/" +
        (form_data.ig_data.fokontany ? form_data.ig_data.fokontany.lib : "") +
        "/" +
        (form_data.ig_data.commune ? form_data.ig_data.commune.lib : "") +
        "/" +
        (form_data.ig_data.district ? form_data.ig_data.district.lib : "") +
        "/" +
        (form_data.ig_data.region ? form_data.ig_data.region.lib : "") +
        "/" +
        (form_data.ig_data.province ? form_data.ig_data.province.lib : ""),
      pict: image_src + req.file.filename,
      coderef: PMT + "-" + form_data.id_data.cin,
      barref: PMT + "-" + form_data.id_data.cin,
    }) as any;
    if (typeof res.then === "function") {
      res.then((result: any) => {
        if ((result && result.id) || result.insertId) {
          const form_model = new FormDataModel();
          const res_f = form_model.insertAllDetails(
            form_data,
            result.id ?? result.insertId
          ) as any;
          if (typeof res_f.then === "function") {
            res_f.then((result_form: any) => {
              response.send(result_form);
            });
          } else {
            form_model.close();
            response.send(res_f);
          }
        } else response.status(400).send(no_data_error(res));
        cards.close();
      });
    } else {
      cards.close();
      response.send(res);
    }
  } else response.status(400).send(body_error);
}
export function SyncSaveData(req: Request, response: Response) {
  if (req.body && req.body.formData) {
    const form_data = JSON.parse(req.body.formData) as FormDataData;
    const cards = new CardItemModel();

    const PMT = Math.floor((Math.random() * 97) + 1)/100;
    const res = cards.add({
      nom: form_data.id_data.nom,
      prenom: form_data.id_data.prenom,
      nis: form_data.id_data.cin,
      cdf: form_data.id_data.cdfm ? 1 : 0,
      adress:
        (form_data.ig_data.lotis ?? "") +
        "/" +
        (form_data.ig_data.fokontany ? form_data.ig_data.fokontany.lib : "") +
        "/" +
        (form_data.ig_data.commune ? form_data.ig_data.commune.lib : "") +
        "/" +
        (form_data.ig_data.district ? form_data.ig_data.district.lib : "") +
        "/" +
        (form_data.ig_data.region ? form_data.ig_data.region.lib : "") +
        "/" +
        (form_data.ig_data.province ? form_data.ig_data.province.lib : ""),
      pict: image_src + req.file.filename,
      coderef: PMT + "-" + form_data.id_data.cin,
      barref: PMT + "-" + form_data.id_data.cin,
    }) as any;
    if (typeof res.then === "function") {
      res.then((result: any) => {
        if ((result && result.id) || result.insertId) {
          const form_model = new FormDataModel();
          const res_f = form_model.insertAllDetails(
            form_data,
            result.id ?? result.insertId,
            true
          ) as any;
          if (typeof res_f.then === "function") {
            res_f.then((result_form: any) => {
              response.send(result_form);
            });
          } else {
            form_model.close();
            response.send(res_f);
          }
        } else response.status(400).send(no_data_error(res));
        cards.close();
      });
    } else {
      cards.close();
      response.send(res);
    }
  } else response.status(400).send(body_error);
}

