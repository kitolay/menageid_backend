import { CardItem } from "./../Models/card";
import { Response, Request } from "express";
import { CardItemModel } from "../Models/card";
import { body_error } from "../common/Response";

export function getCards(req: Request, response: Response) {
  const cards = new CardItemModel();
  if (req.body && req.body.page && req.body.lim) {
    const cards_list = cards.filtered(
      req.body.page,
      req.body.lim,
      req.body.filter
    ) as any;
    if (typeof cards_list.then === "function") {
      cards_list.then((result: CardItem[]) => {
        response.send(result);
      });
    } else {
      cards.close();
      response.send(cards_list);
    }
  } else {
    const cards_list = cards.all() as any;
    if (typeof cards_list.then === "function") {
      cards_list.then((result: CardItem[]) => {
        response.send(result);//http://coursta.mg/menageid/images/profile_pic-1623863667098.png
      });
    } else {
      cards.close();
      response.send(cards_list);
    }
  }
}

export function getDetailData(req: Request, response: Response) {
  const cards = new CardItemModel();
  if (req.query.id) {
    const card_item = cards.getById(parseInt(req.query.id as string)) as any;
    if (typeof card_item.then === "function") {
      card_item.then((card_res: CardItem[]) => {
        if (card_res[0] && card_res[0].id) {
          const card_details = cards.getDetailList(parseInt(req.query.id as string)) as any;
          if(typeof card_details.then === "function"){
            card_details.then((cdetail:any)=>{
              if(cdetail && cdetail !== {}){
                response.send({...card_res[0],details:cdetail});
              }else {
                cards.close();
                response.send(card_res);
              }
            })
          }else {
          cards.close();
          response.send(card_res);
        }
        } else {
          cards.close();
          response.send(card_res);
        }
      });
    } else {
      cards.close();
      response.send(card_item);
    }
  } else {
    cards.close();
    response.status(400).send(body_error);
  }
}
