import { no_data_error } from "./../common/Response";
import { Response, Request } from "express";
import { body_error } from "../common/Response";
import { CommuneData, DistrictData, FokontanyData, PlaceModel, ProvinceData, RegionData } from "../Models/place";

export function getProvince(req: Request, response: Response) {
  const place_data = new PlaceModel();
  const res = place_data.getProvince() as any;
  if (typeof res.then === "function") {
    res.then((resp: ProvinceData[]) => {
      if (resp && resp.length > 0) {
        response.send(resp);
      } else response.status(400).send(no_data_error(res));
    });
  } else {
    place_data.close();
    response.send(res);
  }
}

export function getRegion(req: Request, response: Response) {
  if (req.body) {
    const place_data = new PlaceModel();
    const res = place_data.getRegion(req.body) as any;
    if (typeof res.then === "function") {
      res.then((resp: RegionData[]) => {
        if (resp && resp.length > 0) {
          response.send(resp);
        } else response.status(400).send(no_data_error(res));
      });
    } else {
      place_data.close();
      response.send(res);
    }
  } else {
    response.status(400).send(body_error);
  }
}

export function getDistrict(req: Request, response: Response) {
  if (req.body) {
    const place_data = new PlaceModel();
    const res = place_data.getDistrict(req.body) as any;
    if (typeof res.then === "function") {
      res.then((resp: DistrictData[]) => {
        if (resp && resp.length > 0) {
          response.send(resp);
        } else response.status(400).send(no_data_error(res));
      });
    } else {
      place_data.close();
      response.send(res);
    }
  } else {
    response.status(400).send(body_error);
  }
}

export function getCommune(req: Request, response: Response) {
  if (req.body) {
    const place_data = new PlaceModel();
    const res = place_data.getComunne(req.body) as any;
    if (typeof res.then === "function") {
      res.then((resp: CommuneData[]) => {
        if (resp && resp.length > 0) {
          response.send(resp);
        } else response.status(400).send(no_data_error(res));
      });
    } else {
      place_data.close();
      response.send(res);
    }
  } else {
    response.status(400).send(body_error);
  }
}

export function getFokontany(req: Request, response: Response) {
  if (req.body) {
    const place_data = new PlaceModel();
    const res = place_data.getFokontany(req.body) as any;
    if (typeof res.then === "function") {
      res.then((resp: FokontanyData[]) => {
        if (resp && resp.length > 0) {
          response.send(resp);
        } else response.status(400).send(no_data_error(res));
      });
    } else {
      place_data.close();
      response.send(res);
    }
  } else {
    response.status(400).send(body_error);
  }
}
