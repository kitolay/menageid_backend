export const body_error = {
  error: true,
  message: "No body data.",
};

export const no_data_error = (err: any) => {
  return {
    error: true,
    message: "No data from request.",
    errMessage: err
  };
};

export const expired_data = {
  error: true,
  message: "Query data expired.",
};

export function CommonResponse(data: any) {
  return {
    valide: true,
    data: data,
  };
}
