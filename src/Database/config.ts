import mysql from "mysql2";
import {
  QueryGetByAny,
  QueryInserItem,
  QueryUpdateByAny,
  QueryGetAll,
  QueryRowCount,
  QueryGetByFilter,
  QueryGetLastRowById,
  QueryGetTableNameList,
  QueryGetIdsByTableName,
  QueryGetByTableNameAlldata,
  QueryGetByTableNameFiltered
} from "./queries";

export class Database {
  host?: string;
  user?: string;
  password?: string;
  database_name?: string;
  database: any;

  constructor() {
    this.host = process.env.DB_HOST ?? "localhost";
    this.user = process.env.DB_USER ?? "root";
    this.password = process.env.DB_PASS ?? "Unw4I3lgrcZmIaox";
    this.database_name = process.env.DB_NAME ?? "collect_data_db";
    this.database = mysql.createConnection({
      host: this.host,
      user: this.user,
      password: this.password,
      database: this.database_name,
    });
  }
  connect(): boolean {
    this.database.connect();
    return true;
  }

  disconnect(): boolean {
    this.database.end();
    return true;
  }

  getTables(): Promise<any> {
    return new Promise((res, rej) => {
      this.database.query(
        QueryGetTableNameList(this.database_name ?? ""),
        (err: any, rows: any, fields: any) => {
          if (err) {
            res({
              message: err,
              query: QueryGetTableNameList(this.database_name ?? ""),
            });
          } else {
            res(rows);
          }
        }
      );
    });
  }

  getSyncIds(tableNames: string[], filters?: boolean, filerdata?: number[][]): Promise<any> {
    return new Promise((res, rej) => {
      let result: any[] = [];
      this.getTableIdsBySteps(
        0,
        tableNames.length,
        tableNames,
        result,
        res,
        filters,
        filerdata
      );
    });
  }

  getTableIdsBySteps(
    i: number,
    lim: number,
    tname: string[],
    reslist: any[],
    resolve: (value: any) => void,
    fl?: boolean,
    fldata?: any[]
  ) {
    if (i < lim) {
      this.database.query(
        fl ? (fldata ? QueryGetByTableNameFiltered(tname[i], fldata[i]) : QueryGetByTableNameAlldata(tname[i])) : QueryGetIdsByTableName(tname[i]),
        (err: any, rows: any, fields: any) => {
          if (err) {
            resolve({
              message: err,
              query: fl ? (fldata ? QueryGetByTableNameFiltered(tname[i], fldata[i]) : QueryGetByTableNameAlldata(tname[i])) : QueryGetIdsByTableName(tname[i]),
            });
          } else {
            reslist.push({
              tableName: tname[i],
              [fl ? 'rows':'ids']: fl ? rows : rows.map((r: any) => {
                if (tname[i] === "ig_data" || tname[i] === "enqueteur_data")
                  return r.uid;
                else return r.id;
              }),
            });
            this.getTableIdsBySteps(i + 1, lim, tname, reslist, resolve, fl, fldata);
          }
        }
      );
    } else {
      resolve(reslist);
    }
  }

  getTableIds(tableName: string): Promise<any> {
    return new Promise((res, rej) => {
      this.database.query();
    });
  }

  getByAny(tName: string, filter: any): Promise<any> {
    return new Promise((res, rej) => {
      this.database.query(
        QueryGetByAny(tName, filter),
        (err: any, rows: any, fields: any) => {
          if (err) {
            res({
              message: err,
              query: QueryGetByAny(tName, filter),
            });
          } else {
            res(rows);
          }
        }
      );
    });
  }

  insertItem(tName: string, data: any): Promise<any> {
    return new Promise((res, rej) => {
      this.database.query(
        QueryInserItem(tName, data),
        (err: any, rows: any, fields: any) => {
          console.log("inserting ===> ", tName, rows, err, fields);
          if (err) {
            res({
              message: err,
              query: QueryInserItem(tName, data),
            });
          } else {
            this.database.query(
              QueryGetLastRowById(tName),
              (err: any, rows: any, fields: any) => {
                if (err) {
                  res({
                    message: err,
                    query: QueryGetLastRowById(tName),
                  });
                } else {
                  res(rows);
                }
              }
            );
            res(rows);
          }
        }
      );
    });
  }

  updateByAny(tName: string, ref: any, data: any): Promise<any> {
    return new Promise((res, rej) => {
      this.database.query(
        QueryUpdateByAny(tName, ref, data),
        (err: any, rows: any, fields: any) => {
          if (err) {
            res({
              message: err,
              query: QueryUpdateByAny(tName, ref, data),
            });
          } else {
            res(rows);
          }
        }
      );
    });
  }

  getAll(tName: string): Promise<any> {
    return new Promise((res, rej) => {
      this.database.query(
        QueryGetAll(tName),
        (err: any, rows: any, fields: any) => {
          if (err) {
            res({
              message: err,
              query: QueryGetAll(tName),
            });
          } else {
            res(rows);
          }
        }
      );
    });
  }

  getAllByFilter(
    tName: string,
    page: number,
    lim: number,
    ref: any
  ): Promise<any> {
    return new Promise((res, rej) => {
      this.database.query(
        QueryRowCount(tName),
        (err: any, rows: any, fields: any) => {
          if (err) {
            res({
              message: err,
              query: QueryGetAll(tName),
            });
          } else {
            if (rows[0]["COUNT(*)"] > 0) {
              this.database.query(
                QueryGetByFilter(tName, ref),
                (serr: any, srows: any, sfields: any) => {
                  if (serr) {
                    res({
                      message: serr,
                      query: QueryGetByFilter(tName, ref),
                    });
                  } else {
                    let filtered_data;
                    if (srows.length > lim && lim && page)
                      filtered_data = srows.slice(
                        (page - 1) * lim,
                        lim + (page - 1) * lim
                      );
                    else filtered_data = srows;
                    res({
                      page: page,
                      lim: lim,
                      count: srows.length,
                      data: filtered_data,
                    });
                  }
                }
              );
            } else res(rows);
          }
        }
      );
    });
  }
}
