export function QueryGetByAny(tablename: string, data: any): string {
  const Keys = Object.keys(data);
  const values = Object.values(data);
  const seach_val = Keys.map((k, i) => {
    let nvalue = typeof values[i] === "number" ? values[i] : `\'${values[i]}\'`;
    return `\`${k}\` = ${nvalue}`;
  }).join(" AND ");
  return `SELECT * FROM ${tablename} WHERE ${seach_val}`;
}

export function QueryInserItem(tablename: string, data: any): string {
  const Keys = Object.keys(data)
    .map((c) => `\`${c}\``)
    .join(", ");
  const values = Object.values(data)
    .map((v) => {
      const valtype = typeof v;
      return valtype === "number" ||
        valtype === "bigint" ||
        valtype === "boolean" ||
        valtype === "string" ||
        valtype === "undefined"
        ? valtype === "string"
          ? (v as string)
              .replace(/'/g, "\\'")
              .replace(/"/g, '\\"')
              .replace(/`/g, "\\`")
          : v
        : JSON.stringify(v)
            .replace(/'/g, "\\'")
            .replace(/"/g, '\\"')
            .replace(/`/g, "\\`");
    })
    .map((c) => (typeof c === "number" ? c : `\'${c}\'`))
    .join(", ");
  return `INSERT INTO \`${tablename}\`(${Keys}) VALUES (${values})`;
}

export function QueryUpdateByAny(
  tablename: string,
  ref: any,
  data: any
): string {
  const rKeys = Object.keys(ref);
  const rvalues = Object.values(ref);
  const seach_val = rKeys
    .map((k, i) => {
      let nvalue =
        typeof rvalues[i] === "number" ? rvalues[i] : `\'${rvalues[i]}\'`;
      return `\`${k}\` = ${nvalue}`;
    })
    .join(" AND ");
  const Keys = Object.keys(data);
  const values = Object.values(data);
  const res = Keys.map((k, i) => {
    const value =
      typeof values[i] === "number" ? values[i] : `\'${values[i]}\'`;
    return `\`${k}\`=${value}`;
  }).join(", ");
  return `UPDATE \`${tablename}\` SET ${res} WHERE ${seach_val}`;
}

export function QueryGetAll(tablename: string) {
  return `SELECT * FROM \`${tablename}\``;
}

export function QueryRowCount(tablename: string) {
  return `SELECT COUNT(*) FROM \`${tablename}\``;
}

export function QueryGetByFilter(tablename: string, ref: any) {
  if (ref) {
    const Keys = Object.keys(ref);
    const values = Object.values(ref);
    const seach_val = Keys.map((k, i) => {
      return `UPPER(\`${k}\`) LIKE UPPER('%${values[i]}%')`;
    }).join(" AND ");

    return `SELECT * FROM \`${tablename}\` WHERE ${seach_val}`;
  } else return `SELECT * FROM \`${tablename}\``;
}

export function QueryGetLastRowById(tablename: string) {
  return ` SELECT * FROM \`${tablename}\` ORDER BY \`id\` DESC LIMIT 1;`;
}

export function QueryGetTableNameList(dbname: string): string {
  return `SELECT table_name FROM information_schema.tables WHERE table_schema = '${dbname}';`;
}

export function QueryGetIdsByTableName(tname: string) {
  if (tname === "ig_data" || tname === "enqueteur_data")
    return `SELECT uid FROM \`${tname}\` WHERE 1;`;
  else return `SELECT id FROM \`${tname}\` WHERE 1;`;
}

export function QueryGetByTableNameAlldata(tname: string) {
  return `SELECT * FROM \`${tname}\` WHERE 1;`;
}

export function QueryGetByTableNameFiltered(tname: string, filter: number[]) {
  if (tname === "ig_data" || tname === "enqueteur_data"){
    const filter_query = filter.map((id)=>{
      return `uid = ${id}`;
    }).join(" OR ");
    return `SELECT * FROM \`${tname}\` WHERE ${filter_query};`;
  }else{
    const filter_query = filter.map((id)=>{
      return `id = ${id}`;
    }).join(" OR ");
    return `SELECT * FROM \`${tname}\` WHERE ${filter_query};`;
  }
}
